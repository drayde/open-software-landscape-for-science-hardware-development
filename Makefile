SRC=main.tex
PDF_OBJECTS=$(SRC:.tex=.pdf)

LATEXMK=latexmk
LATEXMK_OPTIONS=-bibtex -pdf -pdflatex="pdflatex -interaction=nonstopmode -synctex=1"

all: clean $(PDF_OBJECTS)

pdf: clean $(PDF_OBJECTS)

%.pdf: %.tex
		@echo Input file: $<
		$(LATEXMK) $(LATEXMK_OPTIONS_EXTRA) $(LATEXMK_OPTIONS) $<

clean:
		$(LATEXMK) -bibtex -C main
		rm -f $(MAIN).pdfsync
		rm -rf *~ *.tmp
		rm -f *.bbl *.blg *.aux *.end *.fls *.log *.out *.fdb_latexmk
