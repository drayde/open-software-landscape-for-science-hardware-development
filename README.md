# Open Software Landscape for Science Hardware Development

We are writing a paper on the open source software landscape for developing hardware. Specifically we are focussing on science hardware, and the tools needed to communicate these hardware designs effectively to other scientists.

In the interest of openness this paper is being written in the open. Anyone is welcome to get involved and be part of the paper. This is the first time we have written a paper like this, there are bound to be teething problems.

You can see the most recent PDF here:

[Download the latest compiled PDF](https://gitlab.com/bath_open_instrumentation_group/open-software-landscape-for-science-hardware-development/-/jobs/artifacts/master/raw/main.pdf?job=compile)

## How to get involved

### If you are comfortable with LaTeX and Git

*If you are NOT comfortable with LaTeX and or Git see sections below.*

To contribute:

1. Fork this repository
1. Edit the text in LaTeX on your fork
1. Open a merge request

### If you are NOT comfortable with LaTeX and or Git

Instead of LaTeX and git, you can download the latest version in a format below and annotate/add to it in any way that is clear (such as track changes in a word processor) or annotate the PDF

[Download the latest compiled PDF](https://gitlab.com/bath_open_instrumentation_group/open-software-landscape-for-science-hardware-development/-/jobs/artifacts/master/raw/main.pdf?job=compile)  
[Download the latest Libre Office version](https://gitlab.com/bath_open_instrumentation_group/open-software-landscape-for-science-hardware-development/-/jobs/artifacts/master/raw/main.odt?job=convert)  
[Download the latest Word version](https://gitlab.com/bath_open_instrumentation_group/open-software-landscape-for-science-hardware-development/-/jobs/artifacts/master/raw/main.docx?job=convert)

Once you have an annotate version you need to share it with us. The best way to do it is to make a GitLab account, [create an issue](https://gitlab.com/bath_open_instrumentation_group/open-software-landscape-for-science-hardware-development/-/issues), and attach the file to the issue.

If you have a problem with creating a GitLab account or attaching file to an issue, feel free to email the changes to Julian Stirling. To minimise spam I am not putting my email address here, but it should be easy to find from my staff page at the University of Bath.

